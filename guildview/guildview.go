package guildview

import (
	"github.com/bwmarrin/discordgo"
	"github.com/diamondburned/tview"
	"gitlab.com/diamondburned/6voice/guildutils"
)

// GuildView is a reactive element that updates itself to reflect the state.
type GuildView struct {
	*tview.TreeView
	LoadFn func(ch *discordgo.Channel)

	// FilterFn is used to determine if a channel should be shown. False is
	// no.
	FilterFn func(ch *discordgo.Channel) bool

	ses *discordgo.Session

	guilds   map[string]*tview.TreeNode
	channels map[string]*tview.TreeNode
}

// New creates a GuildView. This should be called before Open(), as it catches
// the *discordgo.Ready events.
//
// TODO: Add extra handlers for extra events
func New(s *discordgo.Session) *GuildView {
	gv := &GuildView{
		TreeView: tview.NewTreeView(),
		ses:      s,
		guilds:   map[string]*tview.TreeNode{},
		channels: map[string]*tview.TreeNode{},
	}

	root := tview.NewTreeNode("")
	root.SetIndent(0)
	root.SetChildren([]*tview.TreeNode{})

	gv.TreeView.SetGraphics(false)
	gv.TreeView.SetRoot(root)
	gv.TreeView.SetTopLevel(1)
	gv.TreeView.SetBackgroundColor(-1)
	gv.TreeView.SetSelectedFunc(nil)

	s.AddHandler(gv.getReadyHandler())

	return gv
}

func (gv *GuildView) getReadyHandler() func(*discordgo.Session, *discordgo.Ready) {
	return func(d *discordgo.Session, r *discordgo.Ready) {
		tview.QueueUpdateDraw(func() {
			root := gv.TreeView.GetRoot()

			guildChildren := make([]*tview.TreeNode, 0, len(r.Guilds))

			guildutils.SortGuildsReady(r.Settings, r.Guilds)

			for _, g := range r.Guilds {
				n := tview.NewTreeNode("[::b]" + tview.Escape(g.Name) + "[::-]")
				n.SetIndent(0)
				n.SetSelectable(true)
				n.SetReference(g)
				n.SetExpanded(false)
				n.SetSelectedFunc(func() {
					n.SetExpanded(!n.IsExpanded())
				})

				guildChildren = append(guildChildren, n)
				gv.guilds[g.ID] = n

				children := make([]*tview.TreeNode, 0, len(g.Channels))

				for _, c := range g.Channels {
					if gv.FilterFn != nil && !gv.FilterFn(c) {
						continue
					}

					cn := tview.NewTreeNode("# " + tview.Escape(c.Name))
					cn.SetIndent(2)
					cn.SetSelectable(true)
					cn.SetReference(c)
					cn.SetSelectedFunc(func() {
						gv.LoadFn(cn.GetReference().(*discordgo.Channel))
					})

					children = append(children, cn)
					gv.channels[c.ID] = cn
				}

				n.SetChildren(children)
			}

			root.SetChildren(guildChildren)

			if len(guildChildren) > 0 {
				gv.TreeView.SetCurrentNode(guildChildren[0])
			}
		})
	}
}
