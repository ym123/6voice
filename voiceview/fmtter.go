package voiceview

import "strings"

var fmtVoiceStatus = func(mute, deaf, suppress bool) (status string) {
	if mute || deaf || suppress {
		var statuses = make([]string, 0, 3)

		if mute {
			statuses = append(statuses, "Muted")
		}

		if deaf {
			statuses = append(statuses, "Deafened")
		}

		if suppress {
			statuses = append(statuses, "Suppressed")
		}

		return "[red]" + strings.Join(statuses, ",") + "[-]"
	}

	return ""
}
