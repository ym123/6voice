package voiceview

import (
	"github.com/bwmarrin/discordgo"
	"github.com/diamondburned/tview"
)

type VoiceViewItem struct {
	tview.ListItem
	parent *VoiceView

	member   *discordgo.Member
	state    *discordgo.VoiceState
	speaking *discordgo.VoiceSpeakingUpdate

	// Temporary fields, unreliable
	name string
}

func (v *VoiceView) NewVoiceViewItem(vs *discordgo.VoiceState) (*VoiceViewItem, error) {
	i := &VoiceViewItem{
		parent: v,
		state:  vs,
		ListItem: tview.ListItem{
			Shortcut: 0,
			Selected: func() {}, // Todo: assign this something
		},
	}

	if err := i.UpdateMember(); err != nil {
		return nil, err
	}

	if err := i.ReflectNewState(vs); err != nil {
		return i, err
	}

	return i, nil
}

func (i *VoiceViewItem) UpdateMember() (err error) {
	i.member, err = i.parent.ses.State.Member(i.state.GuildID, i.state.UserID)
	if err != nil {
		i.member, err = i.parent.ses.GuildMember(i.state.GuildID, i.state.UserID)
	}

	if err != nil {
		return
	}

	// Force name update
	i.name = tview.Escape(i.member.User.Username)
	if i.member.Nick != "" {
		i.name = tview.Escape(i.member.Nick)
	}

	return
}

func (i *VoiceViewItem) ReflectSpeaking(vsu *discordgo.VoiceSpeakingUpdate) error {
	if i.state != nil && i.state.UserID != vsu.UserID {
		return nil
	}

	if err := i.ReflectNewState(i.state); err != nil {
		return err
	}

	i.speaking = vsu

	if vsu.Speaking {
		i.MainText = "[green]" + i.MainText + "[-]"
	}

	return nil
}

func (i *VoiceViewItem) ReflectNewState(vs *discordgo.VoiceState) error {
	if i.state != nil && i.state.UserID != vs.UserID {
		return nil
	}

	i.state = vs

	if i.name == "" {
		if err := i.UpdateMember(); err != nil {
			return err
		}
	}

	if i.MainText != i.name {
		i.MainText = i.name
	}

	var sec = fmtVoiceStatus(
		vs.Mute || vs.SelfMute,
		vs.Deaf || vs.SelfDeaf,
		vs.Suppress,
	)

	if i.SecondaryText != sec {
		i.SecondaryText = sec
	}

	return nil
}
