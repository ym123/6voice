package main

import (
	"fmt"
	"os"

	"github.com/bwmarrin/discordgo"
	"github.com/diamondburned/tcell"
	"github.com/diamondburned/tview"
	"github.com/gordonklaus/portaudio"
	"gitlab.com/diamondburned/6voice/guildview"
	"gitlab.com/diamondburned/6voice/nostderr"
	"gitlab.com/diamondburned/6voice/primitives"
	"gitlab.com/diamondburned/6voice/voice"
	"gitlab.com/diamondburned/6voice/voiceview"
)

// var d *discordgo.Session

const logFlags = os.O_RDWR | os.O_CREATE | os.O_APPEND | os.O_SYNC

var log *primitives.Logger

func main() {
	// Get the token from $TOKEN
	TOKEN := os.Getenv("TOKEN")
	if TOKEN == "" {
		panic("No tokens given")
	}

	// Initialize TUI
	tview.Initialize()

	// Initialize PortAudio
	if err := portaudio.Initialize(); err != nil {
		panic(err)
	}

	defer portaudio.Terminate()

	// Log into Discord
	s, err := discordgo.New(TOKEN)
	if err != nil {
		panic(err)
	}

	// Create the logging primitive
	log = primitives.NewLogger()
	log.SetBackgroundColor(-1)

	// Create a new voice context
	voiceCtx := voice.New(s)
	voiceCtx.ErrHandler = func(err error) {
		log.Println(err)
	}

	// More primitives
	vv := voiceview.New(s)
	gv := guildview.New(s)
	gv.LoadFn = joinVoice(s, vv, voiceCtx)
	gv.FilterFn = func(ch *discordgo.Channel) bool {
		return ch.Type == discordgo.ChannelTypeGuildVoice
	}

	// Connect to Discord
	fmt.Println("Connecting to Discord...")

	if err := s.Open(); err != nil {
		panic(err)
	}

	defer s.Close()

	// main frame
	mf := tview.NewFlex()
	mf.SetBackgroundColor(-1)
	mf.SetDirection(tview.FlexRow)

	{ // Top split - guild and voice
		f := tview.NewFlex()
		f.SetBackgroundColor(-1)

		{ // Left side panel - guild view
			frame := tview.NewFrame(gv)
			frame.SetBorders(3, 3, 0, 0, 6, 6)
			frame.SetBackgroundColor(-1)

			f.AddItem(frame, 0, 1, true)
		}

		{
			frame := tview.NewFrame(vv)
			frame.SetBorders(3, 3, 0, 0, 6, 6)
			frame.SetBackgroundColor(-1)

			f.AddItem(frame, 0, 1, true)
		}

		mf.AddItem(f, 0, 2, true)
	}

	{ // Bottom split - console
		mf.AddItem(log, 0, 1, true)
	}

	// Set up tview
	tview.ExecApplication(func(a *tview.Application) bool {
		setupUI()

		a.SetBeforeDrawFunc(func(s tcell.Screen) bool {
			s.Clear()
			return false
		})

		a.SetRoot(mf, true).SetFocus(mf)
		return true
	})

	defer func() {
		if r := recover(); r != nil {
			tview.Stop()
			panic(r)
		}
	}()

	// Redirect stderr to the logger
	nostderr.SetWriter(log)
	if err := nostderr.Start(); err != nil {
		log.Println("Failed to capture stderr, instabilities may occur.")
	}

	// Clone the stderr to a file
	f, err := os.OpenFile(os.TempDir()+"/6voice.log", logFlags, 0664)
	if err == nil {
		defer f.Close()
		nostderr.SetClone(f)
	}

	// Start the TUI
	if err := tview.Run(); err != nil {
		fmt.Println("Error spawning TUI:", err.Error())
		panic(err)
	}
}
