package voice

import (
	"fmt"

	"github.com/bwmarrin/dgvoice"
	"github.com/bwmarrin/discordgo"
	"github.com/gordonklaus/portaudio"
)

const (
	// SampleCount is just magic number shit
	// https://opus-codec.org/docs/html_api-1.0.1/group__opus__encoder.html
	SampleCount = 960

	// Channels is the channel count
	Channels = 2

	bufSize = 960 * 2
)

// silence
var empty = make([]int16, 1920)

// Context declares the voice context. Context is re-usable,
// for maintaining one connection at a time.
type Context struct {
	stopSend chan struct{}
	stopRecv chan struct{}

	ses   *discordgo.Session
	Voice *discordgo.VoiceConnection

	outputStream *portaudio.Stream
	inputStream  *portaudio.Stream

	// ErrHandler is the error handler, used for anything that errors in the
	// goroutines or the Close method. This function will block the thread,
	// so you should put it in a goroutine if it blocks.
	ErrHandler func(error)
}

// New creates a new voice context. This does not initialize PortAudio by
// itself.
func New(s *discordgo.Session) *Context {
	ctx := &Context{
		stopSend:   make(chan struct{}),
		stopRecv:   make(chan struct{}),
		ses:        s,
		ErrHandler: func(error) {},
	}

	return ctx
}

// JoinChannel joins a voice channel. This calls the leave method
// automatically.
func (ctx *Context) JoinChannel(ch *discordgo.Channel) error {
	if ch.Type != discordgo.ChannelTypeGuildVoice {
		return fmt.Errorf("Invalid/unsupported channel type: %d", ch.Type)
	}

	if err := ctx.LeaveChannel(); err != nil {
		return err
	}

	// Start joining the voice channel
	v, err := ctx.ses.ChannelVoiceJoin(ch.GuildID, ch.ID, false, false)
	if err != nil {
		return fmt.Errorf("Failed joining channel: %s", err.Error())
	}

	ctx.Voice = v

	// Tell Discord to start sending audio
	ctx.ToggleSpeaking(true)

	if err := ctx.startRecv(); err != nil {
		// Gracefully leave on error
		ctx.Voice.Disconnect()
		return fmt.Errorf("Failed receiving audio: %s", err.Error())
	}

	if err := ctx.startSend(); err != nil {
		ctx.Voice.Disconnect()
		return fmt.Errorf("Failed sending audio: %s", err.Error())
	}

	return nil
}

// ToggleSpeaking toggles speaking
func (ctx *Context) ToggleSpeaking(b bool) {
	if b {
		ctx.Voice.Speaking(true)
		ctx.Voice.OpusSend <- []byte{0xF8, 0xFF, 0xFE}
	} else {
		ctx.Voice.Speaking(false)
	}
}

// LeaveChannel leaves the channel and stops the audio contexts if in one.
func (ctx *Context) LeaveChannel() error {
	if ctx.Voice != nil {
		if err := ctx.Voice.Disconnect(); err != nil {
			return err
		}

		// If VC is not nil, obviously the goroutines would've started,
		// making it safe to just pass in the empty structs
		ctx.stopSend <- struct{}{}
		ctx.stopRecv <- struct{}{}
	}

	return nil
}

// Close closes the context, freeing up resources and safely disconnecting
// from Discord.
func (ctx *Context) Close() {
	// Close the Voice connection
	if ctx.Voice != nil {
		if err := ctx.Voice.Disconnect(); err != nil {
			ctx.ErrHandler(err)
		}
	}

	// Close the PortAudio streams
	close(ctx.stopSend)
	close(ctx.stopRecv)

	if err := ctx.inputStream.Close(); err != nil {
		ctx.ErrHandler(err)
	}

	if err := ctx.outputStream.Close(); err != nil {
		ctx.ErrHandler(err)
	}

	return
}

func (ctx *Context) startSend() error {
	in, s, err := NewInput(2, 48000, 960)
	if err != nil {
		return err
	}

	if err := s.Start(); err != nil {
		return err
	}

	buf := make(chan []int16)
	go dgvoice.SendPCM(ctx.Voice, buf)

	go func() {
		defer close(buf)

	Loop:
		for {
			select {
			case <-ctx.stopSend:
				break Loop
			case buf <- in:
			}

			// TODO(ym): I don't think this is actually safe, but copying is too expensive
			if err := s.Read(); err != nil {
				ctx.ErrHandler(err)
			}
		}

		// if err := ctx.inputStream.Stop(); err != nil {
		if err := ctx.inputStream.Stop(); err != nil {
			ctx.ErrHandler(err)
		}
	}()

	return nil
}

func (ctx *Context) startRecv() error {
	recv := make(chan *discordgo.Packet, 46)
	go dgvoice.ReceivePCM(ctx.Voice, recv)
	out, s, err := NewOutput(2, 48000, 960)
	if err != nil {
		return err
	}

	if err := s.Start(); err != nil {
		return err
	}

	go func() {
		defer close(recv)

	Loop:
		for {
			select {
			case i := <-recv:
				copy(out, i.PCM)
			case _ = <-ctx.stopRecv:
				break Loop
			default:
				copy(out, empty) // TODO(ym)
			}

			if err := s.Write(); err != nil {
				ctx.ErrHandler(err)
			}
		}

		if err := ctx.outputStream.Stop(); err != nil {
			ctx.ErrHandler(err)
		}
	}()

	return nil
}
