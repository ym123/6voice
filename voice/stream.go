package voice

import (
	"errors"
	"log"

	"github.com/gordonklaus/portaudio"
)

type StreamType int

const (
	Input StreamType = iota
	Output
)

type Stream interface {
	Read() error
	Write() error
	Close() error
	Stop() error
	Start() error
}

type ResamplingStream struct {
	Stream
	sType        StreamType
	sChan, iChan int
	sampleRateL  int
	sampleRateM  int
	ibuf         []int16
	obuf         []int16
}

func (s *ResamplingStream) Write() error {
	if s.sType != Output {
		return errors.New("Cannot write to an input stream!")
	}
	copy(s.obuf, s.ibuf)
	s.Stream.Write()
	return nil
}

func (s *ResamplingStream) Read() error {
	if s.sType != Input {
		return errors.New("Cannot read from an output stream!")
	}
	s.Stream.Read()
	copy(s.ibuf, s.obuf)
	return nil
}

func Gcd(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

func Min(f, s int) int {
	if f > s {
		return s
	}
	return f
}

// TODO(ym): actually search for devices lol or allow the user to choose one, for now we use the default one
func NewOutput(channels, sampleRate, samplesPerBuf int) ([]int16, Stream, error) {
	// remove this branch for testing lol
	d, err := portaudio.DefaultOutputDevice()
	if err != nil {
		return nil, nil, err
	}

	// params := portaudio.StreamParameters{
	// 	portaudio.StreamDeviceParameters{},
	// 	portaudio.StreamDeviceParameters{d, channels, d.DefaultLowOutputLatency},
	// 	float64(sampleRate), samplesPerBuf * channels, portaudio.NoFlag,
	// }

	out := make([]int16, channels*samplesPerBuf)
	// err = portaudio.IsFormatSupported(params, &out)
	// if err == nil {
	// 	s, err := portaudio.OpenStream(params, &out)
	// 	if err != nil {
	// 		return nil, nil, err
	// 	}
	// 	return out, s, nil
	// }
	// log.Println(err.Error())

	noChannels := Min(channels, d.MaxOutputChannels)
	pOut := make([]int16, noChannels*samplesPerBuf)
	s, err := portaudio.OpenDefaultStream(0, noChannels, d.DefaultSampleRate, samplesPerBuf*noChannels, &pOut)
	if err != nil {
		return nil, nil, err
	}
	log.Println("Max output channels: ", d.MaxOutputChannels, ", Output channels: ", noChannels, ", Default Sample Rate: ", d.DefaultSampleRate)
	gcd := Gcd(int(d.DefaultSampleRate), sampleRate)
	stream := &ResamplingStream{
		obuf:        pOut,
		ibuf:        out,
		sType:       Output,
		sChan:       noChannels,
		iChan:       channels,
		sampleRateL: sampleRate / gcd,
		sampleRateM: int(d.DefaultSampleRate) / gcd,
		Stream:      s,
	}
	return out, stream, nil
}

func NewInput(channels, sampleRate, samplesPerBuf int) ([]int16, Stream, error) {
	d, err := portaudio.DefaultInputDevice()
	if err != nil {
		return nil, nil, err
	}

	// params := portaudio.StreamParameters{
	// 	portaudio.StreamDeviceParameters{d, channels, d.DefaultLowInputLatency},
	// 	portaudio.StreamDeviceParameters{},
	// 	float64(sampleRate), samplesPerBuf * channels, portaudio.NoFlag,
	// }

	out := make([]int16, channels*samplesPerBuf)
	// err = portaudio.IsFormatSupported(params, out)
	// if err == nil {
	// 	s, err := portaudio.OpenStream(params, out)
	// 	if err != nil {
	// 		return nil, nil, err
	// 	}
	// 	return out, s, nil
	// }
	// log.Println(err.Error())

	noChannels := Min(channels, d.MaxInputChannels)
	pIn := make([]int16, noChannels*samplesPerBuf)
	s, err := portaudio.OpenDefaultStream(
		noChannels, 0, 48000,
		samplesPerBuf*noChannels, pIn,
	)

	if err != nil {
		return nil, nil, err
	}

	log.Println("Max input channels:", d.MaxInputChannels)
	log.Println("Input channels:", noChannels)
	log.Println("Default Sample Rate: ", 48000)

	gcd := Gcd(int(48000), sampleRate)
	stream := &ResamplingStream{
		obuf:        pIn,
		ibuf:        out,
		sType:       Input,
		sChan:       noChannels,
		iChan:       channels,
		sampleRateL: sampleRate / gcd,
		sampleRateM: int(d.DefaultSampleRate) / gcd,
		Stream:      s,
	}
	return out, stream, nil
}
