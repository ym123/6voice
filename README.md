# 6voice

## Checker list

- [ ] `voiceview/voiceview.go:L164` should account for self, so that `v.states == 0` never happens
- [ ] The right voice box should be auto-updated on channel list hover
- [ ] The channel list should display who is in the voice chat
- [ ] Actually finish the application

