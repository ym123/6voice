module gitlab.com/diamondburned/6voice

go 1.12

require (
	github.com/bwmarrin/dgvoice v0.0.0-20170706020935-3c939eca8b2f
	github.com/bwmarrin/discordgo v0.16.1-0.20190528235223-789616715332
	github.com/diamondburned/tcell v1.1.7-0.20190608162241-468b8880ec7a
	github.com/diamondburned/tview v1.2.2-0.20190609100116-f44377c613dc
	github.com/gordonklaus/portaudio v0.0.0-20180817120803-00e7307ccd93
	golang.org/x/crypto v0.0.0-20190611184440-5c40567a22f8 // indirect
	golang.org/x/sys v0.0.0-20190610200419-93c9922d18ae // indirect
	layeh.com/gopus v0.0.0-20161224163843-0ebf989153aa // indirect
)
