package error

import (
	"fmt"
	"runtime"
	"strings"
)

func genTrace(level int) string {
	var traces = make([]string, 0, level)

	for i := 1; i < level+1; i++ {
		_, fn, line, ok := runtime.Caller(i)
		if !ok {
			break
		}

		traces = append(traces, fmt.Sprintf(
			"%d - %s:%d", i, fn, line,
		))
	}

	return strings.Join(traces, "\n")
}
