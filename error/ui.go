package error

import (
	"log"
	"runtime/debug"

	"github.com/diamondburned/tview"
)

var modal *tview.Modal

var (
	// ContinueLabel is the label used for the Continue button
	ContinueLabel = "Continue"
)

// Modal pops up a modal dialog
func Modal(err error) {
	if modal == nil {
		modal = tview.NewModal()
		modal.AddButtons([]string{ContinueLabel})
		modal.SetBackgroundColor(-1)
	}

	var trace = genTrace(3)

	modal.SetText(trace + "\n" + err.Error())

	var old tview.Primitive
	tview.ExecApplication(func(a *tview.Application) bool {
		old = a.GetRoot()
		return false
	})

	modal.SetDoneFunc(func(_ int, label string) {
		switch label {
		case ContinueLabel:
			tview.ExecApplication(func(a *tview.Application) bool {
				a.SetRoot(old, true)
				return true
			})
		}
	})

	tview.ExecApplication(func(a *tview.Application) bool {
		a.SetRoot(modal, true)
		return true
	})

	log.Printf(
		"\n\n%sAbove stack trace for error %s\n",
		string(debug.Stack()), err.Error(),
	)
}
