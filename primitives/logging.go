package primitives

import (
	"log"
	"strings"
	"sync"

	"github.com/diamondburned/tview"
)

// Logger is a tview widget used for reactive logging
type Logger struct {
	*tview.TextView

	logger *log.Logger
	mu     sync.Mutex

	panicFn func()
}

// NewLogger creates a new logger
func NewLogger() *Logger {
	tv := tview.NewTextView()
	tv.SetDynamicColors(true)

	return &Logger{
		TextView: tv,
		logger:   log.New(tv, "[grey]", log.Ltime),
	}
}

// SetPanicFn sets the function to run when Panic* is called
func (l *Logger) SetPanicFn(fn func()) {
	l.panicFn = fn
}

// Fatal prints to logger with the red color
func (l *Logger) Fatal(v ...interface{}) {
	defer l.setMarkup("[red]")()
	l.logger.Print(v...)
}

// Fatalf prints to logger with the red color
func (l *Logger) Fatalf(fmt string, v ...interface{}) {
	defer l.setMarkup("[red]")()
	l.logger.Printf(fmt, v...)
}

// Fatalln prints to logger with the red color
func (l *Logger) Fatalln(v ...interface{}) {
	defer l.setMarkup("[red]")()
	l.logger.Println(v...)
}

// Panic prints to logger with the red color, then executes the panic
// function if there's any
func (l *Logger) Panic(v ...interface{}) {
	defer l.setMarkup("[red]")()
	l.logger.Print(v...)
	if l.panicFn != nil {
		l.panicFn()
	}
}

// Panicf prints to logger with the red color, then executes the panic
// function if there's any
func (l *Logger) Panicf(fmt string, v ...interface{}) {
	defer l.setMarkup("[red]")()
	l.logger.Printf(fmt, v...)
	if l.panicFn != nil {
		l.panicFn()
	}
}

// Panicln prints to logger with the red color, then executes the panic
// function if there's any
func (l *Logger) Panicln(v ...interface{}) {
	defer l.setMarkup("[red]")()
	l.logger.Println(v...)
	if l.panicFn != nil {
		l.panicFn()
	}
}

// Print prints to logger with the grey color
func (l *Logger) Print(v ...interface{}) {
	defer l.lock()()
	l.logger.Print(v...)
}

// Printf prints to logger with the grey color
func (l *Logger) Printf(fmt string, v ...interface{}) {
	defer l.lock()()
	l.logger.Printf(fmt, v...)
}

// Println prints to logger with the grey color
func (l *Logger) Println(v ...interface{}) {
	defer l.lock()()
	l.logger.Println(v...)
}

func (l *Logger) Write(p []byte) (n int, err error) {
	for _, line := range strings.Split(string(p), "\n") {
		if line == "" {
			continue
		}

		l.Println(line)
	}

	return len(p), nil
}

func (l *Logger) lock() func() {
	l.mu.Lock()
	return l.mu.Unlock
}

func (l *Logger) setMarkup(f string) func() {
	l.mu.Lock()
	l.logger.SetPrefix(f)
	return func() {
		l.logger.SetPrefix("[grey]")
		l.mu.Unlock()
	}
}
