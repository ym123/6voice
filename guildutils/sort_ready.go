package guildutils

import (
	"sort"

	"github.com/bwmarrin/discordgo"
)

// SortGuildsReady sorts all guilds in `r.Guilds' accordingly.
func SortGuildsReady(s *discordgo.Settings, gs []*discordgo.Guild) {
	// Sort accordingly to the preference
	sort.Slice(gs, func(a, b int) bool {
		aFound := false
		for _, guild := range s.GuildPositions {
			if aFound {
				if guild == gs[b].ID {
					return true
				}
			} else {
				if guild == gs[a].ID {
					aFound = true
				}
			}
		}

		return false
	})
}

// SortPMsReady sorts all private messaging channels in `r.PrivateChannels'.
func SortPMsReady(s *discordgo.Session, r *discordgo.Ready) {
	sort.Slice(r.PrivateChannels, func(a, b int) bool {
		channelA := r.PrivateChannels[a]
		channelB := r.PrivateChannels[b]

		return channelA.LastMessageID > channelB.LastMessageID
	})
}
